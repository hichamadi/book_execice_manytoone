from time import sleep  # Sleep make the server waits

import psycopg2  # PostgreSQL adapter
from fastapi import FastAPI, status, HTTPException  # FastAPI Module + status + HTTPException
from psycopg2.extras import RealDictCursor  # Add names of fields to cursor
from pydantic import BaseModel  # Schema

# Database Connection
while True:
    try: # Try / except to catch the errors
        connexion = psycopg2.connect(
                host='localhost',
                port=5432,
                database='manytoone',
                #database='postgres',
                user='postgres',
                password='API', # Good password
                cursor_factory=RealDictCursor)
        cursor= connexion.cursor()
        print('Database connection: successful')
        break # If connection succeed, break the loop
    except Exception as error:
        print('Database connection: failed')
        print('Error: ', error)
        sleep(2) # If connection fails, retry in 2 seconds

# FastAPI server
app = FastAPI() # API instance name @app.get("/posts")

# Select all blogpost records
@app.get("/posts")
def get_posts():
    # Writing the SQL query
    cursor.execute("SELECT * FROM book")
    # Retrieving all the posts (list/array)
    database_posts = cursor.fetchall()
    return {"data": database_posts}

# Select a book record from its id
@app.get('/posts/{id_uri}')
def get_post(id_uri: int):
    cursor.execute("SELECT * FROM book WHERE id=%i" % id_uri)
    corresponding_post = cursor.fetchone()
    if not corresponding_post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Not corresponding post was found with id:{id_uri}")
    return {"data": corresponding_post}

# Pydantic schema for Book Body validation
class BlogPost(BaseModel):
    title: str
    author: str
    writer_id: int



# Insert a new blogpost record
@app.post('/posts', status_code=status.HTTP_201_CREATED)
def create_post(post_body: BlogPost):
    try:

        cursor.execute("INSERT INTO book (title, author, writer_id) "
                    "VALUES(%s, %s,  %s) RETURNING *;",
                    (post_body.title, post_body.author, post_body.writer_id ))
        new_post = cursor.fetchone()
        connexion.commit(); # Save the changes to the Database
        #return new_post
        return {"data" :new_post}
    except psycopg2.errors.ForeignKeyViolation as err:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        detail=f"Foreign key violation with writer_id:{post_body.writer_id}")

# Update a blogpost record from its id
@app.put('/posts/{id_uri}')
def update_post(id_uri: int, post_body: BlogPost):
    try:
        cursor.execute("UPDATE book SET title=%s, content=%s , writer_id=%s WHERE id=%s RETURNING *;",
                        (post_body.title, post_body.content, str(id_uri)))
        updated_post = cursor.fetchone()
        connexion.commit()
        if not updated_post:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_uri}")
        return {"data": updated_post}
    except psycopg2.errors.ForeignKeyViolation as err:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        detail=f"Foreign key violation with writer_id:{post_body.writer_id}")

# Delete a blogpost record from its id
@app.delete('/posts/{id_uri}', status_code=status.HTTP_204_NO_CONTENT)
def delete_post(id_uri:int):
    cursor.execute("DELETE FROM book WHERE id=%i RETURNING *;" % id_uri)
    deleting_post = cursor.fetchone()
    connexion.commit()  # Save the changes to the Database
    if not deleting_post:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_uri}")
    return {"data": deleting_post}
